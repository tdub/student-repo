- name: Playbook to configure ansible controller 
  hosts: localhost
  connection: local
  vars:
    user: "{{ username }}"
     
  environment:
    CONTROLLER_HOST: 'ansible-1'
    CONTROLLER_USERNAME: 'admin'
    CONTROLLER_PASSWORD: '{{ password }}' 
    CONTROLLER_VERIFY_SSL: false

  tasks:

  - name: Create projects
    ansible.controller.project:
      name: Student Project
      organization: 'Red Hat network organization'
      scm_url: "https://gitlab.com/{{user}}/student-repo.git"
      scm_type: git
      allow_override: true
      state: present
      scm_update_on_launch: yes

  - name: Add group
    ansible.controller.group:
      name: cisco
      variables:
        ansible_connection: network_cli
        ansible_network_os: cisco.ios.ios
      inventory: "Workshop Inventory"
      state: present

  - name: Add group
    ansible.controller.group:
      name: arista
      variables:
        ansible_connection: network_cli
        ansible_network_os: arista.eos.eos 
        ansible_become: 'true'
        ansible_become_method: enable
      inventory: "Workshop Inventory"

  - name: Add group
    ansible.controller.group:
      name: juniper
      variables:
        ansible_connection: netconf
        ansible_network_os: junipernetworks.junos.junos
      inventory: "Workshop Inventory"
      state: present


  - name: Create Execution Environments
    ansible.controller.execution_environment:
      organization: 'Red Hat network organization'
      name: "Validated Network"
      image: registry.gitlab.com/redhatautomation/validated-network-ee:latest
      pull: missing

  -  name: Gitlab Credential 
     ansible.controller.credential_type:
       injectors:
          extra_vars:
            email: "{{ email | default (' {{ email }}') }}"
            token: "{{ token | default ('{{ token }}') }}"
            username: "{{ username | default ('{{ username }}') }}"
       inputs:
          fields:
            - id: username
              label: Username
              type: string
            - id: token
              label: Token
              secret: true
              type: string
            - id: email
              label: Email
              type: string
          required:
            - username
            - token
            - email
       kind: cloud
       name: Gitlab Student
# This section includes 1-opportunistic/2-backups-and-restore solution/Exercise2
  -  name: Gitlab Credential 
     ansible.controller.credential:
      credential_type: Gitlab Student
      description: ''
      inputs:
          username: '{{ username }}'
          token: '{{ token }}'
          email: example@example.com
      name: Gitlab Credential 
      organization: 'Red Hat network organization'

  - name: Create a job-template - Network-Backups-Git
    ansible.controller.job_template:
      name: "Network-Backups-Git"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/2-backup-and-restore/1-backups.yml"
      credentials:
        - "Workshop Credential"
        - "Gitlab Credential"
      execution_environment: "Validated Network" 
      state: "present"
# This section includes 1-opportunistic/2-backups-and-restore solution/Exercise3
  - name: Create a job-template - Network-Backups-Server
    ansible.controller.job_template:
      name: "Network-Backups-Server"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/2-backup-and-restore/2-backups.yml"
      credentials:
        - "Workshop Credential"
        - "Controller Credential"
      execution_environment: "network workshop execution environment" 
      state: "present"

  - name: Create a job-template - Network-Automation-Restore
    ansible.controller.job_template:
      name: "Network-Automation-Restore"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/2-backup-and-restore/4-network_restore.yml"
      credentials:
        - "Workshop Credential"
        - "Controller Credential"
      execution_environment: "network workshop execution environment" 
      state: "present"

  - name: Create a job-template - Network-Change
    ansible.controller.job_template:
      name: "Network-Change"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/2-backup-and-restore/3-change.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "Validated Network" 
      state: "present" 

  - name: Create a job-template - Network-Verify
    ansible.controller.job_template:
      name: "Network-Verify"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/2-backup-and-restore/5-verify.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "Validated Network" 
      state: "present" 

  - name: Create a workflow job template 
    ansible.controller.workflow_job_template:
      name: Network - Backup and Restore Workflow
      inventory: 'Workshop Inventory'
      organization: "Red Hat network organization"
      state: present

  - name: Node Network-Verify
    ansible.controller.workflow_job_template_node:    
      identifier: Network-Verify
      workflow: 'Network - Backup and Restore Workflow'
      unified_job_template: Network-Verify
      state: present  

  - name: Node Network-Automation-Restore
    ansible.controller.workflow_job_template_node:       
      identifier: Network-Automation-Restore
      workflow: 'Network - Backup and Restore Workflow'
      unified_job_template: Network-Automation-Restore
      success_nodes: Network-Verify
      state: present

  - name: Node Network-Change
    ansible.controller.workflow_job_template_node:
      identifier: Network-Change
      workflow: 'Network - Backup and Restore Workflow'
      unified_job_template: Network-Change
      success_nodes: Network-Automation-Restore
      state: present

  - name: Node Network-Backups-Server
    ansible.controller.workflow_job_template_node:
      identifier: Network-Backups-Server
      workflow: 'Network - Backup and Restore Workflow'
      unified_job_template: Network-Backups-Server
      success_nodes: Network-Change
      state: present
# This section includes 1-opportunistic/3-dynamic-documentation
       
  - name: Create a job-template - Network-Config
    ansible.controller.job_template:
      name: "Network-Config"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/3-dynamic-documentation/config.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "Validated Network" 
      state: "present" 
  
  - name: Create a job-template - Network-Compliance-Dashboard
    ansible.controller.job_template:
      name: "Network-Compliance-Dashboard"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/1-opportunistic/3-dynamic-documentation/solution/network_report.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "network workshop execution environment" 
      state: "present"
  
# This section includes 2-systematic/4-network-compliance
  - name: Create a job-template - Network-Check-Compliance
    ansible.controller.job_template:
      name: "Network-Check-Compliance"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/2-systematic/4-network-compliance/solution/compliance.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "network workshop execution environment" 
      job_type: check
      state: "present" 
  
  - name: Create a job-template - Network-Run-Compliance
    ansible.controller.job_template:
      name: "Network-Run-Compliance"
      organization: 'Red Hat network organization'
      inventory: "Workshop Inventory"
      project: "Student Project"
      playbook: "day2/2-systematic/4-network-compliance/solution/compliance.yml"
      credentials:
        - "Workshop Credential"
      execution_environment: "network workshop execution environment" 
      job_type: check
      state: "present" 

  - name: Create a workflow job template 
    ansible.controller.workflow_job_template:
      name: Network-Compliance-Workflow
      inventory: 'Workshop Inventory'
      organization: "Red Hat network organization"
      state: present
      survey_enabled: true
      survey:
          name: ''
          description: ''
          spec:
            - question_name: Which groups/devices to run checks against?
              type: multiselect
              variable: _group
              required: true
              default:  ''
              choices:
                - cisco
                - arista
                - juniper
            - question_name: Please select the desired compliance roles?
              type: multiselect
              variable: tag
              required: true
              default: ''
              choices:
                - logging
                - snmp 
                - ntp

  - name: Node Network-Run-Compliance
    ansible.controller.workflow_job_template_node:    
      identifier: Network-Run-Compliance
      workflow: 'Network-Compliance-Workflow'
      unified_job_template: Network-Run-Compliance
      organization: 'Red Hat network organization'
      state: present
      
  - name: Node Change Approval
    ansible.controller.workflow_job_template_node:    
      identifier: 'Change Approval'
      workflow_job_template: 'Network-Compliance-Workflow'
      organization: 'Red Hat network organization'
      approval_node: 
        name: 'Change Approval'
        timeout: 0
      state: present    

  - name: Node Change Approval
    ansible.controller.workflow_job_template_node:    
      identifier: 'Change Approval'
      workflow_job_template: 'Network-Compliance-Workflow'
      organization: 'Red Hat network organization'
      success_nodes: 
      - Network-Run-Compliance
      state: present    

  - name: Node Network-Check-Compliance
    ansible.controller.workflow_job_template_node:    
      identifier: Network-Check-Compliance
      workflow: 'Network-Compliance-Workflow'
      unified_job_template: Network-Check-Compliance
      organization: 'Red Hat network organization'
      success_nodes: 'Change Approval'
      state: present