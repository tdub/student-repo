# Exercise 3 - Inventories

## Table of Contents

- [Exercise 3 - Inventories](#exercise-3---inventories)
  - [Table of Contents](#table-of-contents)
  - [Objective](#objective)
  - [Diagram](#diagram)
  - [Guide](#guide)
    - [Step 1 - Examining inventory](#step-1---examining-inventory)
      - [Static Inventory Files](#static-inventory-files)
    - [Step 2 - Using ansible-navigator to explore inventory](#step-2---using-ansible-navigator-to-explore-inventory)
    - [Step 3 - Connecting to network devices](#step-3---connecting-to-network-devices)
    - [Step 4 - Adhoc Pings](#step-4---adhoc-pings)
  - [Exercise Challenges](#exercise-challenges)
  - [Complete](#complete)

## Objective
The objective for this exercise it to understand the use of inventories in Ansible, which are going to provide you with the endpints that you are automating.


## Diagram

![Red Hat Ansible Automation](https://github.com/ansible/workshops/raw/devel/images/ansible_network_diagram.png)

## Guide

### Step 1 - Examining inventory

#### Static Inventory Files

The scope of a `play` within a `playbook` is limited to the groups of hosts declared within an Ansible **inventory**. Ansible supports multiple [inventory](http://docs.ansible.com/ansible/latest/intro_inventory.html) types. An inventory could be a static flat file with a collection of hosts defined within it or it could be a dynamic script that querys a system such as a CMDB for a list of devices and associated variables to run a play against.

In this lab you will work with a file based static inventory written in the **ini** format. Either use Visual Studio Code to open or use the `cat` command to view the contents of the `~/lab_inventory/hosts` file.

```bash
$ cat ~/lab_inventory/hosts
```

```ini
[all:vars]
ansible_ssh_private_key_file=~/.ssh/aws-private.pem

[routers:children]
cisco
juniper
arista

[cisco]
rtr1 ansible_host=18.222.121.247 private_ip=172.16.129.86
[arista]
rtr2 ansible_host=18.188.194.126 private_ip=172.17.158.197
rtr4 ansible_host=18.221.5.35 private_ip=172.17.8.111
[juniper]
rtr3 ansible_host=3.14.132.20 private_ip=172.16.73.175

[cisco:vars]
ansible_user=ec2-user
ansible_network_os=ios
ansible_connection=network_cli

[juniper:vars]
ansible_user=ec2-user
ansible_network_os=junos
ansible_connection=netconf

[arista:vars]
ansible_user=ec2-user
ansible_network_os=eos
ansible_connection=network_cli
ansible_become=true
ansible_become_method=enable

[dc1]
rtr1
rtr3

[dc2]
rtr2
rtr4

[control]
ansible ansible_host=13.58.149.157 ansible_user=student private_ip=172.16.240.184
```

In the above output every `[ ]` defines a group. For example `[dc1]` is a group that contains the hosts `rtr1` and `rtr3`. Groups can also be _nested_. The group `[routers]` is a parent group to the group `[cisco]`

Parent groups are declared using the `children` directive. Having nested groups allows the flexibility of assigining more specific values to variables.

We can associate variables to groups and hosts.

> Note:
>
> A group called **all** always exists and contains all groups and hosts defined within an inventory.

Host variables can be defined on the same line as the host themselves. For example for the host `rtr1`:

```sh
rtr1 ansible_host=18.222.121.247 private_ip=172.16.129.86
```

* `rtr1` - The name that Ansible will use.  This can but does not have to rely on DNS
* `ansible_host` - The IP address that ansible will use, if not configured it will default to DNS
* `private_ip` - This value is not reserved by ansible so it will default to a [host variable](http://docs.ansible.com/ansible/latest/intro_inventory.html#host-variables).  This variable can be used by playbooks or ignored completely.

Group variables groups are declared using the `vars` directive. Having groups allows the flexibility of assigning common variables to multiple hosts. Multiple group variables can be defined under the `[group_name:vars]` section. For example look at the group `cisco`:

```sh
[cisco:vars]
ansible_user=ec2-user
ansible_network_os=ios
ansible_connection=network_cli
```

* `ansible_user` - The user ansible will be used to login to this host, if not configured it will default to the user the playbook is run from
* `ansible_network_os` - This variable is necessary while using the `network_cli` connection type within a play definition, as we will see shortly.
* `ansible_connection` - This variable sets the [connection plugin](https://docs.ansible.com/ansible/latest/plugins/connection.html) for this group.  This can be set to values such as `netconf`, `httpapi` and `network_cli` depending on what this particular network platform supports.

We will work more with host and group variables in the next exercise.


### Step 2 - Using ansible-navigator to explore inventory

We can also use the `ansible-navigator` TUI to explore inventory.

Run the `ansible-navigator inventory` command to bring up inventory in the TUI:

![ansible-navigator tui](images/ansible-navigator.png)

Pressing **0** or **1** on your keyboard will open groups or hosts respectively.

![ansible-navigator groups](images/ansible-navigator-groups.png)

Press the **Esc** key to go up a level, or you can zoom in to an individual host:

![ansible-navigator host](images/ansible-navigator-rtr-1.png)

### Step 3 - Connecting to network devices

There are four routers, named rtr1, rtr2, rtr3 and rtr4.  The network diagram is always available on the [network automation workshop table of contents](../README.md).  The SSH configuration file (`~/.ssh/config`) is already setup on the control node.  This means you can SSH to any router from the control node without a login:

For example to connect to rtr1 from the Ansible control node, type:

```bash
$ ssh rtr1
```

For example:
```
$ ssh rtr1
Warning: Permanently added 'rtr1,35.175.115.246' (RSA) to the list of known hosts.

rtr1#show ver
Cisco IOS XE Software, Version 16.09.02
```

### Step 4 - Adhoc Pings

Now we are going to have Ansible use the inventory for some basic connectivity testing.

Because we have already installed the required modules in exercise 1, we can just run our commands now (if you didn't finish exercise 1, this won't work).  We are going to use the ansible ping module; if we are able to connect to the remote system (connect and authenticate with the endpoint) it will return a 'pong'.  

We will start off using the ping module to connect to rtr1.  Below you see that we are using -m to declare the module we want to use and then the host that we want to run against from our inventory.  You can ignore the warning.

```bash
ansible -m ping -i ~/lab_inventory/hosts rtr1
[WARNING]: ansible-pylibssh not installed, falling back to paramiko
rtr1 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}
```

If successful, you should see `SUCCESS`, as well as a `pong` response next to the `ping`.

## Exercise Challenges

Using Groups  
In the last step we were able to ping a single router.  In this challenge you need to determine how to use your inventory effectively to target specific endpoints.
* How would we ping all of the routers?
* How would we ping just the EOS routers?
* How would we ping just the JunOS routers?


Configure a New Group and Group Vars
- Add a group to the inventory (~/lab_inventory/hosts) called cisco_sandbox.
- Add a host to the group: `sandbox-iosxe-latest-1.cisco.com`
- Create a group variables section, and define required variables
  - ansible_user=admin
  - ansible_password=Cisco12345
  - ansible_connection=network_cli
  - ansible_network_os= <what ansible_network_os setting should you use?>
- Ping the router using the Ansible ping module.

## Complete

You have completed lab exercise 3 - Inventories!  

You now understand:
* Where the inventory is stored for command-line exercises
* How groups and variables are annotated in the inventory file
* How to use ansible-navigator TUI (Text-based user interface)
* How to use `ansible` ad-hoc commands with the inventory to target specifc hosts and groups

---
[Previous Exercise](../2-RolesAndCollections/README.md) |
[Next Exercise](../4-Playbooks/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)
