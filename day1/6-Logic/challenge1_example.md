- name: Cisco IOS XE STIG
  hosts: cisco
  gather_facts: no
  vars:
    ignore_all_errors: true
    iosxeSTIG_stigrule_215814_Manage: True
    iosxeSTIG_stigrule_215814_login_Text: "some message"

  roles:
    - name: iosxeSTIG