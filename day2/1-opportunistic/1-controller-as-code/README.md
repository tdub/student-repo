## Return to Day 2 Exercises
* [Day 2 ](../../README.md)

# Exercise 1 - Controller-as-Code

## Table of Contents
- [Step 1 - Variables](#step-1-variables)
- [Step 2 - Using the Terminal](#step-2-using-the-terminal)
- [Step 3 - Run Playbook](#step-3-run-playbook)
- [Step 4 - Examine the Ansible Controller configuration](#step-4-examine-the-ansible-controller-configuration)

## Objective

Explore and understand Controller-as-Code.

## Overview

In this exercise use the ansible.controller collection to configure the AAP Controller. This lab will utilize the VSCode as part of your student workshop environment. By modifify and running a playbook from the ansible-navigtor CLI, we can avoid the AAP Controller GUI to configure additional elements needed for the subsequent exercise-2. 

### Collections
* [ansible.controller](https://github.com/ansible/awx)

### Validated Role
* [infra.controller_configuration](https://github.com/redhat-cop/controller_configuration)

### Step 1 - Variables

- Connect to Visual Studio Code from the Workshop launch page (provided by your instructor).  The password is provided below the WebUI link.

  ![launch page](../../images/launch_page.png)

- Type in the provided password to connect.

  ![login vs code](../../images/vscode_login.png)

1. Open the `student-repo` directory in the browser tab for Visual Studio Code and navigate to open setup.yml:
2. File Explorer - Open folder -> /student-repo/
3. Locate and review the day2/1-opportunistic/1-controller-as-code/setup.yml
4. Examine the playbook tasks


### Step 2 - Using the Terminal

- Open a terminal in Visual Studio Code:

Navigate to the `1-controller-as-code` directory on the Ansible control node terminal.

```bash
[student@ansible-1 1-controller-as-code]$ ls
export.yml  setup.yml  solution  vars
[student@ansible-1 1-controller-as-code]$ pwd
/home/student/student-repo/day2/1-opportunistic/1-controller-as-code
```

### Step 3 - Run Playbook 
The setup.yml playbook will require extra variables when running in ansible-navigator.

- {{ username }} is your gitlab username
- {{ token }} is your gitlab token, which you saved to ~/gitlab_token.txt in the 1-GettingStarted exercise.
- The CONTROLLER_PASSWORD is the same password assigned to your POD for the Ansible Controller

> If for some reason on day1 you didn't complete Day 1 - Exercise 1 - Getting Started, click [here](../../../day1/1-GettingStarted/README.md) and complete it now.

### If you are good to go from Day1, start here for Day 2 Exercise 1
Run the `ansible-navigator` command with the `run` argument and -m stdout as well as  -e for the `--extra-vars`

- The setup.yml playbook will configure the following in the Automation Controller.
  - A new Project named 'Student Project'
  - A new Execution Environment named 'Validate Network'
  - A new Credential Type named 'Gitlab Credential'

  ~~~
  [student@ansible-1 1-controller-as-code]$ ansible-navigator run setup.yml -m stdout -e "username=your-gitlab-user token=your-gitlab-token password=your-lab-student-password"
  ~~~

#### Output
The first run will show 'changed'
```bash
[student@ansible-1 1-controller-as-code]$ ansible-navigator run setup.yml -m stdout -e "username=your-gitlab-user token=your-gitlab-token password=your-lab-student-password"

PLAY [Playbook to configure ansible controller] ********************************

TASK [Gathering Facts] *********************************************************
ok: [localhost]

TASK [Create projects] *********************************************************
ok: [localhost]

TASK [Add group] ***************************************************************
ok: [localhost]

TASK [Add group] ***************************************************************
ok: [localhost]

TASK [Add group] ***************************************************************
ok: [localhost]

TASK [Create Execution Environments] *******************************************
ok: [localhost]

TASK [Gitlab Credential] *******************************************************
ok: [localhost]

PLAY RECAP *********************************************************************
localhost                  : ok=7    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0    
```

### Step 4 - Examine the Ansible Controller configuration
#### Please note some pre-configured job-templates and other configurations from the Ansible Network Automation Workshop for beginners are visible in your AAP controller.

Access your AAP from the https://xxxxx.example.opentlc.com/ link. 
- Your link is shared by the instructor and will be slightly different each workshop.

Your AAP Controller should now show the following new configurations:

#### Student Project
The `Student Project` will enable the Ansible Controller to pull files from your Gitlab repo `student-repo`.

 ![student project](../../images/studentproject.png)

 #### Credential Type
 A new credential type `Gitlab Student` was created to populate the variables used for the ansible.scm collection needed for exercise-2.

 The credential type effectively provides the input needed and extra variable output needed for the execise-2 gitlab credential 

  ![gitlab credential](../../images/credentialtype.png)

  #### Execution Environment
  The Validated Network EE includes the ansible.scm, ansible.network and other validated 
  collections needed for the subsequent exercises.

  ![validated network](../../images/validatednetwork.png)

# Congratulations, Exercise-1 is complete!

## Next Exercise
* [Backup-and-Restore ](../2-backup-and-restore/README.md)

[Click Here to return to the Ansible Network Automation Workshop](../README.md)