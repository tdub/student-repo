### The setup.yml playbook will configure the all of the necessary controller constructs up to 6-automated-netops-validation

Run the `ansible-navigator` command with the `run` argument and -m stdout as well as `extra-vars` -e

- **Note the cloud_pass variable**

```bash
$ ansible-navigator run setup.yml -m stdout -e "username=your-gitlab-user token=your-gitlab-token password=your-lab-student-password cloud_pass=instuctor_provided" 